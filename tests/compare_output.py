#!/usr/bin/env python3
#
# gperf compare_output.py
#
# Copyright (C) 2020 Tim-Philipp Müller <tim centricular com>
#
# This file is part of GNU GPERF.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Compares two text files line-by-line to make sure they match.

import os
import sys

if len(sys.argv) != 3:
  print('ERROR: usage: {} EXPECTED-OUTPUT-FILE ACTUAL-TEST-OUTPUT-FILE'.format(sys.argv[0]))
  sys.exit(-1)

fn_exp = sys.argv[1]
fn_test = sys.argv[2]
f_exp = open(fn_exp, 'r', encoding='utf8')
f_test = open(fn_test, 'r', encoding='utf8')

for (n, (line_exp, line_test)) in enumerate(zip(f_exp, f_test)):
  line_exp = line_exp.strip()
  line_test = line_test.strip()
  if line_exp != line_test:
    print('Output does not match in line {}:\nExpected: "{}" in {}\nActual:   "{}" in {}'.format(n+1, line_exp, fn_exp, line_test, fn_test))
    sys.exit(-1)

if f_exp.read() != '':
  print('Actual output file "{}" has fewer lines than expected out file "{}"!'.format(fn_test, fn_exp))
  sys.exit(-1)

if f_test.read() != '':
  print('Actual output file "{}" has more lines than expected out file "{}"!'.format(fn_test, fn_exp))
  sys.exit(-1)

f_exp.close()
f_test.close()

sys.exit(0)
