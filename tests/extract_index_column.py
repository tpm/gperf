#!/usr/bin/env python3
#
# gperf extract_index_column.py
#
# Copyright (C) 2020 Tim-Philipp Müller <tim centricular com>
#
# This file is part of GNU GPERF.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Takes .gperf file and strips the 'struct' bit at the top and all but the
# first column of data, e.g. to extract just foreign_name from lang-utf8.gperf

import os
import sys

if len(sys.argv) < 2:
  print('ERROR: usage: {} INPUT.GPERF [OUTPUT.GPERF]'.format(sys.argv[0]))
  sys.exit(-1)

input_fn = sys.argv[1]
f = open(input_fn, 'r', encoding='utf8')

if len(sys.argv) > 2 and sys.argv[2] != '-':
  f_out = open(sys.argv[2], 'w', encoding='utf8')
else:
  f_out = sys.stdout

seen_hdr_sep = False

for (n, (line)) in enumerate(f):
  line = line.strip()
  if not seen_hdr_sep:
    seen_hdr_sep = line == '%%'
    continue
  first_column = line.split(',')[0]
  print(first_column, file=f_out)

sys.exit(0)
