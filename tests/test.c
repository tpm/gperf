/*
   Tests the generated perfect hash function.
   The -v option prints diagnostics as to whether a word is in
   the set or not.  Without -v the program is useful for timing.

   Usage: $executable [-v] [INPUT.GPERF]

   If no input file is specified it will read from stdin.
*/

#include <stdio.h>
#include <string.h>

extern const char * in_word_set (const char *, size_t);

#define MAX_LEN 80

int
main (int argc, char *argv[])
{
  const char *input_fn = NULL;
  int  verbose = 0;
  char buf[MAX_LEN];
  FILE *f = NULL;

  if (argc > 2) {
    verbose = 1;
    input_fn = argv[2];
  } else if (argc == 2) {
    if (strcmp (argv[1], "-v") == 0)
      verbose = 1;
    else
      input_fn = argv[1];
  }

  if (input_fn != NULL && strcmp (input_fn, "-") != 0) {
    f = fopen (argv[2], "r");
    if (f == NULL) {
      perror("Could not open input file: ");
      return -1;
    }
  } else {
    f = stdin;
  }

  while (fgets (buf, MAX_LEN, f))
    {
      if (strlen (buf) > 0 && buf[strlen (buf) - 1] == '\n')
        buf[strlen (buf) - 1] = '\0';

      if (in_word_set (buf, strlen (buf)))
        {
          if (verbose)
            printf ("in word set %s\n", buf);
        }
      else
        {
          if (verbose)
            printf ("NOT in word set %s\n", buf);
        }
    }

  return 0;
}
